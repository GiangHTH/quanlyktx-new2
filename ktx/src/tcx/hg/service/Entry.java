
package tcx.hg.service;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.config.ResourceNotFoundException;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.zkoss.util.resource.Labels;
import org.zkoss.zhtml.Object;

import tcx.hg.cms.service.ChuDeService;
import tcx.hg.cms.service.HopDongService;
/*import vn.toancauxanh.cms.service.BaiVietService;
import vn.toancauxanh.cms.service.BannerService;
import vn.toancauxanh.cms.service.CategoryService;
import vn.toancauxanh.cms.service.DonViHanhChinhService;
import vn.toancauxanh.cms.service.DonViService;
import vn.toancauxanh.cms.service.ImageService;
import vn.toancauxanh.cms.service.LanguageService;
import vn.toancauxanh.cms.service.ThamSoService;*/
import tcx.hg.cms.service.HouseService;
import tcx.hg.cms.service.RoomService;
import tcx.hg.cms.service.SinhVienService;
import tcx.hg.model.VaiTro;

@Configuration
@Controller
public class Entry extends BaseObject<Object> {
	static Entry instance;

	@Value("${trangthai.apdung}")
	public String TT_AP_DUNG = "";
	@Value("${trangthai.daxoa}")
	public String TT_DA_XOA = "";
	@Value("${trangthai.khongapdung}")
	public String TT_KHONG_AP_DUNG = "";

	// No image url
	public String URL_M_NOIMAGE = "/assetsfe/images/lg_noimage.png";
	public String URL_S_NOIMAGE = "/assetsfe/images/sm_noimage.png";
	
	//trang thai bai viet
	@Value("${trangthaisoan.daduyet}")
	public String TTS_DA_DUYET = "";
	@Value("${trangthaisoan.dangsoan}")
	public String TTS_DANG_SOAN = "";
	@Value("${trangthaisoan.choduyet}")
	public String TTS_CHO_DUYET = "";
	@Value("${trangthaisoan.tuchoi}")
	public String TTS_TU_CHOI = "";
	@Value("${trangthaihienthi.tieudiemchinh}")
	public String TT_TIEU_DIEM_CHINH = "";
	@Value("${trangthaihienthi.noibat}")
	public boolean TT_NOI_BAT = true;
	
	@Value("${action.xem}")
	public String XEM = ""; // duoc xem bat ky
	@Value("${action.list}")
	public String LIST = ""; // duoc vao trang list search url
	@Value("${action.sua}")
	public String SUA = ""; // duoc sua bat ky
	@Value("${action.xoa}")
	public String XOA = ""; // duoc xoa bat ky
	@Value("${action.them}")
	public String THEM = ""; // duoc them
	@Value("${action.xuatban}")
	public String XUATBAN = ""; // duoc duyet va xuat ban
	@Value("${action.lichsucapnhat}")
	public String LICHSUCAPNHAT = ""; // duoc them
	@Value("${action.lichsu}")
	public String LICHSU = "";

	@Value("${url.nhanvien}")
	public String NHANVIEN = "";
	@Value("${url.vaitro}")
	public String VAITRO = "";
	@Value("${url.banner}")
	public String BANNER = "";
	@Value("${url.baiviet}")
	public String BAIVIET = "";
	@Value("${url.house}")
	public String HOUSE = "";
	@Value("${url.room}")
	public String ROOM = "";
	@Value("${url.sinhvien}")
	public String SINHVIEN = "";
	@Value("${url.hopdong}")
	public String HOPDONG = "";
	@Value("${url.chude}")
	public String CHUDE = "";
	// uend
	public char CHAR_CACH = ':';
	public String CACH = CHAR_CACH + "";

	@Value("${url.vaitro}" + ":" + "${action.xem}")
	public String VAITROXEM;
	@Value("${url.vaitro}" + ":" + "${action.them}")
	public String VAITROTHEM = "";
	@Value("${url.vaitro}" + ":" + "${action.list}")
	public String VAITROLIST = "";
	@Value("${url.vaitro}" + ":" + "${action.xoa}")
	public String VAITROXOA = "";
	@Value("${url.vaitro}" + ":" + "${action.sua}")
	public String VAITROSUA = "";

	@Value("${url.nhanvien}" + ":" + "${action.xem}")
	public String NHANVIENXEM = "";
	@Value("${url.nhanvien}" + ":" + "${action.them}")
	public String NHANVIENTHEM = "";
	@Value("${url.nhanvien}" + ":" + "${action.list}")
	public String NHANVIENLIST = "";
	@Value("${url.nhanvien}" + ":" + "${action.xoa}")
	public String NHANVIENXOA = "";
	@Value("${url.nhanvien}" + ":" + "${action.sua}")
	public String NHANVIENSUA = "";

	@Value("${url.quantrihethong}" + ":" + "${action.list}")
	public String QUANTRIHETHONGLIST = "";

	// Hệ thống active
	@Value("${url.hethong}" + ":" + "${action.xem}")
	public String HETHONGXEM = "";
	@Value("${url.hethong}" + ":" + "${action.sua}")
	public String HETHONGSUA = "";

	@Value("${url.banner}" + ":" + "${action.xem}")
	public String BANNERXEM = "";
	@Value("${url.banner}" + ":" + "${action.them}")
	public String BANNERTHEM = "";
	@Value("${url.banner}" + ":" + "${action.list}")
	public String BANNERLIST = "";
	@Value("${url.banner}" + ":" + "${action.xoa}")
	public String BANNERXOA = "";
	@Value("${url.banner}" + ":" + "${action.sua}")
	public String BANNERSUA = "";

	@Value("${url.chude}" + ":" + "${action.xem}")
	public String CHUDEXEM = "";
	@Value("${url.chude}" + ":" + "${action.them}")
	public String CHUDETHEM = "";
	@Value("${url.chude}" + ":" + "${action.list}")
	public String CHUDELIST = "";
	@Value("${url.chude}" + ":" + "${action.xoa}")
	public String CHUDEXOA = "";
	@Value("${url.chude}" + ":" + "${action.sua}")
	public String CHUDESUA = "";

	@Value("${url.baiviet}" + ":" + "${action.xem}")
	public String BAIVIETXEM = "";
	@Value("${url.baiviet}" + ":" + "${action.them}")
	public String BAIVIETTHEM = "";
	@Value("${url.baiviet}" + ":" + "${action.list}")
	public String BAIVIETLIST = "";
	@Value("${url.baiviet}" + ":" + "${action.xoa}")
	public String BAIVIETXOA = "";
	@Value("${url.baiviet}" + ":" + "${action.sua}")
	public String BAIVIETSUA = "";
	@Value("${url.baiviet}" + ":" + "${action.xuatban}")
	public String BAIVIETXUATBAN = "";
	
	@Value("${url.thongke}" + ":" + "${action.list}")
	public String THONGKELIST = "";
	
	@Value("${url.house}" + ":" + "${action.xem}")
	public String HOUSEXEM = "";
	@Value("${url.house}" + ":" + "${action.them}")
	public String HOUSETHEM = "";
	@Value("${url.house}" + ":" + "${action.list}")
	public String HOUSELIST = "";
	@Value("${url.house}" + ":" + "${action.xoa}")
	public String HOUSEXOA = "";
	@Value("${url.house}" + ":" + "${action.sua}")
	public String HOUSESUA = "";
	
	@Value("${url.room}" + ":" + "${action.xem}")
	public String ROOMXEM = "";
	@Value("${url.room}" + ":" + "${action.them}")
	public String ROOMTHEM = "";
	@Value("${url.room}" + ":" + "${action.list}")
	public String ROOMLIST = "";
	@Value("${url.room}" + ":" + "${action.xoa}")
	public String ROOMXOA = "";
	@Value("${url.room}" + ":" + "${action.sua}")
	public String ROOMSUA = "";
	
	@Value("${url.sinhvien}" + ":" + "${action.xem}")
	public String SINHVIENXEM = "";
	@Value("${url.sinhvien}" + ":" + "${action.them}")
	public String SINHVIENTHEM = "";
	@Value("${url.sinhvien}" + ":" + "${action.list}")
	public String SINHVIENLIST = "";
	@Value("${url.sinhvien}" + ":" + "${action.xoa}")
	public String SINHVIENXOA = "";
	@Value("${url.sinhvien}" + ":" + "${action.sua}")
	public String SINHVIENSUA = "";
	
	@Value("${url.hopdong}" + ":" + "${action.xem}")
	public String HOPDONGXEM = "";
	@Value("${url.hopdong}" + ":" + "${action.them}")
	public String HOPDONGTHEM = "";
	@Value("${url.hopdong}" + ":" + "${action.list}")
	public String HOPDONGLIST = "";
	@Value("${url.hopdong}" + ":" + "${action.xoa}")
	public String HOPDONGXOA = "";
	@Value("${url.hopdong}" + ":" + "${action.sua}")
	public String HOPDONGSUA = "";

	// aend
	public String[] getRESOURCES() {
		return new String[] { NHANVIEN, VAITRO, BANNER, CHUDE, HOUSE, ROOM, SINHVIEN,HOPDONG};
	}

	public String[] getACTIONS() {
		return new String[] { LIST, XEM, THEM, SUA, XOA, XUATBAN, LICHSUCAPNHAT, LICHSU };
	}

	static {
		File file = new File(Labels.getLabel("filestore.root") + File.separator + Labels.getLabel("filestore.folder"));
		if (!file.exists()) {
			if (file.mkdir()) {
				System.out.println("Directory mis is created!");
			} else {
				System.out.println("Failed to create directory!");
			}
		}
	}
	@Autowired
	public Environment env;

	@Autowired
	DataSource dataSource;

	public Entry() {
		super();
		setCore();
		instance = this;
	}

	@Bean
	public FilterRegistrationBean cacheFilter() {
		FilterRegistrationBean rs = new FilterRegistrationBean(new CacheFilter());
		rs.addUrlPatterns("*.css");
		rs.addUrlPatterns("*.js");
		rs.addUrlPatterns("*.wpd");
		rs.addUrlPatterns("*.wcs");
		rs.addUrlPatterns("*.jpg");
		rs.addUrlPatterns("*.jpeg");
		rs.addUrlPatterns("*.png");
		rs.addUrlPatterns("*.svg");
		rs.addUrlPatterns("*.gif");
		return rs;
	}

	/* @Bean
	 public FilterRegistrationBean loginFilter() {
	 FilterRegistrationBean rs = new FilterRegistrationBean(new
	 LoginFilter());
	 rs.addUrlPatterns("/cp*");
	 return rs;
	 }*/

	Map<String, String> pathMap = new HashMap<>();
	{
		MapUtils.putAll(pathMap,
				new String[] { "doanhnghiep", "doanhnghiep", "gioithieu", "gioithieu", "cosophaply", "cosophaply" });
	}

	@RequestMapping(value = "/")
	public String home() {
		return "forward:/frontend/index.zhtml?resource=hopdong&file=/frontend/home/hopdong/home.zhtml";
	}
	
	@RequestMapping(value = "/{path:.+$}")
	public String newDetail(@PathVariable String path) {
		return "forward:/frontend/index.zhtml?resource="+path+"&file=/frontend/home/"+path+"/home.zhtml";
	}
	
	/*@RequestMapping(value = "/{path:.+$}/{cat:\\d+}")
	public String newDetail(@PathVariable String path, @PathVariable Long cat) {
		return "forward:/frontend/index.zhtml?resource="+path+"&file=/frontend/kyhop/newslist.zhtml&cat="
				+ cat;
	}*/
	
	// BE
	@RequestMapping(value = "/cp")
	public String cp() {
		return "forward:/WEB-INF/zul/home1.zul?resource=nhanvien&action=lietke&file=/WEB-INF/zul/nhanvien/list.zul&macdinh=home";
	}
	
	@RequestMapping(value = "/cp/{path:.+$}")
	public String cp(@PathVariable String path) {
		return "forward:/WEB-INF/zul/home1.zul?resource=" + path + "&action=lietke&file=/WEB-INF/zul/" + path
				+ "/list.zul";
	}

	@RequestMapping(value = "/login")
	public String dangNhapBackend() {
		return "forward:/WEB-INF/zul/login.zul";
	}
	
	/*public final DonViService getDonVis() {
		return new DonViService();
	}*/
	
	public final Quyen getQuyen() {
		return getNhanVien().getTatCaQuyen();
	}
	public final VaiTroService getVaiTros() {
		return new VaiTroService();
	}
	/*public final DonViHanhChinhService getDonViHanhChinhs() {
		return new DonViHanhChinhService();
	}
	
	public final CategoryService getCategories() {
		return new CategoryService();
	}
	
	public final BaiVietService getBaiViets() {
		return new BaiVietService();
	}
	
	public final ImageService getImages(){
		return new ImageService();
	}
	
	public final ThamSoService getThamSos(){
		return new ThamSoService();
	}
	
	public final BannerService getBanners(){
		return new BannerService();
	}*/
	
	public final HouseService getHouses(){
		return new HouseService();
	}
	
	public final RoomService getRooms(){
		return new RoomService();
	}
	
	public final NhanVienService getNhanViens(){
		return new NhanVienService();
	}
	
	public final SinhVienService getSinhViens(){
		return new SinhVienService();
	}
	
	public final HopDongService getHopDongs(){
		return new HopDongService();
	}
	
	public final ChuDeService getChuDes(){
		return new ChuDeService();
	}

	public final List<String> getNoiDungActive() {
		return Arrays.asList("chude", "baiviet", "video", "gallery", "linhvuchoidap", "hoidaptructuyen", "faqcategory",
				"faq");
	}
	
	/*public final LanguageService getLanguages() {
		return new LanguageService();
	}*/
	public boolean checkVaiTro(String vaiTro) {
		if (vaiTro == null || vaiTro.isEmpty()) {
			return false;
		}
		boolean rs = false;
		for (VaiTro vt : getNhanVien().getVaiTros()) {
			if (vaiTro.equals(vt.getAlias())) {
				rs = true;
				break;
			}
		}
		return rs;// || getQuyen().get(vaiTro);
	}
	
	@Configuration
	@EnableWebMvc
	public static class MvcConfig extends WebMvcConfigurerAdapter {
	    @Override
	    public void addResourceHandlers(ResourceHandlerRegistry registry) {
	    	registry
	          .addResourceHandler("/files/**")
	          .addResourceLocations("file:/home/ktxdata/ktxfiles/");
	        registry
	          .addResourceHandler("/assetsfe/**")
	          .addResourceLocations("/assetsfe/");
	        registry
	          .addResourceHandler("/backend/**")
	          .addResourceLocations("/backend/");
	        registry
	          .addResourceHandler("/img/**")
	          .addResourceLocations("/img/");
	        registry
	          .addResourceHandler("/login/**")
	          .addResourceLocations("/login/");
	    }
	    
	    @Override
	    public void configureViewResolvers(final ViewResolverRegistry registry) {
	        registry.jsp("/WEB-INF/", "*");
	    }
	    @ExceptionHandler(ResourceNotFoundException.class)
	    @ResponseStatus(HttpStatus.NOT_FOUND)
	    public String handleResourceNotFoundException() {
	        return "forward:/WEB-INF/zul/notfound.zul";
	    }
	}

}