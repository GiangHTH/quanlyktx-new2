package tcx.hg.gg.model;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.Nullable;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import tcx.hg.model.Model;

@Entity
@Table(name = "HopDong")
public class HopDong extends Model<HopDong> {
	private SinhVien sinhvien;
	private Room phongtro;
	private @Nullable Date ngayBatDau; // ngày bắt đầu ở trọ
	private @Nullable Date ngayKetThuc; // ngày kết thúc ở trọ
	private @Nullable Date ngayTaoHD; // ngày chủ nhà tạo hợp đồng
	// nếu sinh viên đki trước trên mạng r mới tới chủ nhà lập HĐ
	// thì ngày đki sẽ đc lưu trong thuộc tính "ngayTao"
	private @Nullable Date ngayHuyHD; // ngày chủ nhà kết thúc hợp đồng
	private String soTien; // số tiền phải trả trong thời gian ở trọ
	private String ghiChu; // chủ trọ ghi chú thêm những việc liên quan đến hợp
							// đồng này

	@ManyToOne
	public SinhVien getSinhvien() {
		return sinhvien;
	}

	public void setSinhvien(SinhVien sinhvien) {
		this.sinhvien = sinhvien;
	}

	@ManyToOne
	public Room getPhongtro() {
		return phongtro;
	}

	public void setPhongtro(Room phongtro) {
		this.phongtro = phongtro;
	}

	public @Nullable Date getNgayBatDau() {
		return ngayBatDau;
	}

	public void setNgayBatDau(Date ngayBatDau) {
		this.ngayBatDau = ngayBatDau;
	}

	public @Nullable Date getNgayKetThuc() {
		return ngayKetThuc;
	}

	public void setNgayKetThuc(Date ngayKetThuc) {
		this.ngayKetThuc = ngayKetThuc;
	}

	public @Nullable Date getNgayTaoHD() {
		return ngayTaoHD;
	}

	public void setNgayTaoHD(Date ngayTaoHD) {
		this.ngayTaoHD = ngayTaoHD;
	}

	public @Nullable Date getNgayHuyHD() {
		return ngayHuyHD;
	}

	public void setNgayHuyHD(Date ngayHuyHD) {
		this.ngayHuyHD = ngayHuyHD;
	}

	public String getSoTien() {
		return soTien;
	}

	public void setSoTien(String soTien) {
		this.soTien = soTien;
	}

	public String getGhiChu() {
		return ghiChu;
	}

	public void setGhiChu(String ghiChu) {
		this.ghiChu = ghiChu;
	}

	@Command
	public void saveHopDong(@BindingParam("list") final Object listObject, @BindingParam("attr") final String attr,
			@BindingParam("wdn") final Window wdn) throws IOException {
		if (NGAYTAO == null) {
			//tạo hđ lần đầu (đki +tạo hđ)
			//sinh viên đã được chọn hoặc đã đăng kí và phải chọn phòng trọ
			if (sinhvien != null && phongtro != null) {
				//kiểm tra xem sinh viên đó đã tạo hợp đồng ở đâu đó chưa
				if (find(HopDong.class).where(QHopDong.hopDong.trangThai.ne(core().TT_DA_XOA))
						.where(QHopDong.hopDong.sinhvien.id.eq(sinhvien.getId()))
						.where(QHopDong.hopDong.ngayTaoHD.isNotNull()).where(QHopDong.hopDong.ngayHuyHD.isNull())
						.fetchCount() > 0) {
					Messagebox.show(
							"Sinh viên chưa hủy hợp đồng đã tạo ở lần trước, phải hủy hợp đồng trước khi muốn tạo hợp đồng mới",
							"Thông báo", Messagebox.OK, Messagebox.INFORMATION);
				} else {
					//kiểm tra xem sinh viên đó đã đăng kí phòng chưa, k cho đăng kí cùng lúc nhiều phòng
					if (find(HopDong.class).where(QHopDong.hopDong.trangThai.ne(core().TT_DA_XOA))
							.where(QHopDong.hopDong.sinhvien.id.eq(sinhvien.getId()))
							.where(QHopDong.hopDong.ngayTaoHD.isNull()).where(QHopDong.hopDong.ngayHuyHD.isNull())
							.fetchCount() > 0) {
						Messagebox.show(
								"Sinh viên này đã đăng kí phòng, nếu muốn đăng kí lại phải hủy đăng kí ở phòng này trước đó",
								"Thông báo", Messagebox.OK, Messagebox.ERROR);
					} else {
						//kiểm tra xem số ng đang ở và đã đăng kí đã vượt đạt mức số ng tối đa chưa
						if ((find(HopDong.class).where(QHopDong.hopDong.trangThai.ne(core().TT_DA_XOA))
								.where(QHopDong.hopDong.phongtro.id.eq(phongtro.getId()))
								.where(QHopDong.hopDong.ngayTaoHD.isNotNull())
								.where(QHopDong.hopDong.ngayHuyHD.isNull())
								.fetchCount() + 
								find(HopDong.class).where(QHopDong.hopDong.trangThai.ne(core().TT_DA_XOA))
								.where(QHopDong.hopDong.phongtro.id.eq(phongtro.getId()))
								.where(QHopDong.hopDong.ngayTaoHD.isNull())
								.where(QHopDong.hopDong.ngayHuyHD.isNull())
								.fetchCount()) >=
								phongtro.getMaxPeople()) {
							Messagebox.show("Hết chỗ", "Cảnh báo", Messagebox.OK,
						            Messagebox.ERROR);
						} else {
							//ngày bắt đầu phải sau ngày hôm nay và ngày kết thúc phải sau ngày bắt đầu
							if (ngayKetThuc.after(ngayBatDau) && (ngayBatDau.after(new Date()) || ngayBatDau.equals(new Date()))) {
								// tính tiền trọ, tính số ngày ở lại rồi nhân với số tiền
								// trọ 1 ngày
								Calendar c1 = Calendar.getInstance();
								Calendar c2 = Calendar.getInstance();
								c1.setTime(getNgayBatDau());
								c2.setTime(getNgayKetThuc());
								long money = ((c2.getTime().getTime() - c1.getTime().getTime()) / (24 * 3600 * 1000)) * 15000;
								setSoTien(Long.toString(money));
								// .tính tiền trọ, tiền trọ 1 ngày là 15000 đ
								save();
								Messagebox.show("Số tiền phải trả là: " + getSoTien() + " VNĐ", "Thông báo", Messagebox.OK,
										Messagebox.INFORMATION);
								
								wdn.detach();
								BindUtils.postNotifyChange(null, null, this, "*");
								BindUtils.postNotifyChange(null, null, listObject, attr);

							} else {
								if (!ngayKetThuc.after(ngayBatDau) && ngayBatDau.before(new Date())) {
									showNotification("Ngày bắt đầu ở phải là ngày hôm nay hoặc lớn hơn và ngày kết thúc phải sau ngày bắt đầu ở, mời chọn lại thời gian", "", "error");
								} else{
									if(!ngayKetThuc.after(ngayBatDau)){
										showNotification("Ngày kết thúc phải sau ngày bắt đầu ở, mời chọn lại thời gian", "", "error");
									}
									if(ngayBatDau.before(new Date())){
										showNotification("Ngày bắt đầu ở phải là ngày hôm nay hoặc lớn hơn, mời chọn lại thời gian", "", "error");
									}
								}
							}
						}
					}
				}
			} else {
				if (sinhvien == null && phongtro == null) {
					showNotification("Chưa chọn sinh viên và phòng trọ", "", "error");
				} else{
					if(sinhvien == null){
						showNotification("Chưa chọn sinh viên", "", "error");
					}
					if(phongtro == null){
						showNotification("Chưa chọn phòng trọ", "", "error");
					}
				}
			}
			
		}else{
			//đã tạo hợp đồng trước đó
			//khi chỉnh sửa hợp đồng (tạo hđ từ hđ đã đkí) thì k cần kiểm tra thời gian
			//ngayTaoHD == null nghĩa là sinh viên mới chỉ đăng kí, lúc này quản lý sẽ kích nút "lưu hợp đồng" để tạo hợp đồng
			//tức sẽ cập nhật lại ngày tạo HĐ
			if (ngayTaoHD == null) {
				setNgayTaoHD(new Date());
			}
			//khi chỉnh sửa hợp đồng (tạo hđ từ hđ đã đkí) thì k cần kiểm tra đã hủy hợp đồng trước chưa, đã đkí phòng nào trước chưa hoặc đã vượt số ng cho phép chưa
			// tính tiền trọ, tính số ngày ở lại rồi nhân với số tiền
			// trọ 1 ngày
			Calendar c1 = Calendar.getInstance();
			Calendar c2 = Calendar.getInstance();
			c1.setTime(getNgayBatDau());
			c2.setTime(getNgayKetThuc());
			long money = ((c2.getTime().getTime() - c1.getTime().getTime()) / (24 * 3600 * 1000)) * 15000;
			setSoTien(Long.toString(money));
			// .tính tiền trọ, tiền trọ 1 ngày là 15000 đ
			save();
			Messagebox.show("Số tiền phải trả là: " + getSoTien() + " VNĐ", "Thông báo", Messagebox.OK,
					Messagebox.INFORMATION);
			
			wdn.detach();
			BindUtils.postNotifyChange(null, null, this, "*");
			BindUtils.postNotifyChange(null, null, listObject, attr);
		}
	}

	@Command
	public void dangkiHopDong(@BindingParam("sinhvien") final SinhVien sinhvien,
							  @BindingParam("list") final Object listObject, 
							  @BindingParam("attr") final String attr) throws IOException {
		setSinhvien(sinhvien);
		if (ngayKetThuc.after(ngayBatDau) && (ngayBatDau.after(new Date()) || ngayBatDau.equals(new Date()))) {
			if (phongtro != null) {
				//kiểm tra xem sinh viên đó đã tạo hợp đồng ở đâu đó chưa
				if (find(HopDong.class).where(QHopDong.hopDong.trangThai.ne(core().TT_DA_XOA))
						.where(QHopDong.hopDong.sinhvien.id.eq(sinhvien.getId()))
						.where(QHopDong.hopDong.ngayTaoHD.isNotNull()).where(QHopDong.hopDong.ngayHuyHD.isNull())
						.fetchCount() > 0) {
					Messagebox.show(
							"Bạn chưa hủy hợp đồng đã tạo ở lần trước, phải hủy hợp đồng trước khi muốn tạo hợp đồng mới",
							"Thông báo", Messagebox.OK, Messagebox.ERROR);
				} else {
					//kiểm tra xem sinh viên đó đã đăng kí phòng chưa, k cho đăng kí cùng lúc nhiều phòng
					if (find(HopDong.class).where(QHopDong.hopDong.trangThai.ne(core().TT_DA_XOA))
							.where(QHopDong.hopDong.sinhvien.id.eq(sinhvien.getId()))
							.where(QHopDong.hopDong.ngayTaoHD.isNull()).where(QHopDong.hopDong.ngayHuyHD.isNull())
							.fetchCount() > 0) {
						Messagebox.show(
								"Bạn đã đăng kí phòng, nếu muốn đăng kí lại mời xóa đăng kí trước đó",
								"Thông báo", Messagebox.OK, Messagebox.ERROR);
					} else {
						//kiểm tra xem số ng đang ở và đã đăng kí đã vượt đạt mức số ng tối đa chưa
						if ((find(HopDong.class).where(QHopDong.hopDong.trangThai.ne(core().TT_DA_XOA))
								.where(QHopDong.hopDong.phongtro.id.eq(phongtro.getId()))
								.where(QHopDong.hopDong.ngayTaoHD.isNotNull())
								.where(QHopDong.hopDong.ngayHuyHD.isNull())
								.fetchCount() + 
								find(HopDong.class).where(QHopDong.hopDong.trangThai.ne(core().TT_DA_XOA))
								.where(QHopDong.hopDong.phongtro.id.eq(phongtro.getId()))
								.where(QHopDong.hopDong.ngayTaoHD.isNull())
								.where(QHopDong.hopDong.ngayHuyHD.isNull())
								.fetchCount()) >=
								phongtro.getMaxPeople()) {
							Messagebox.show("Hết chỗ", "Cảnh báo", Messagebox.OK,
						            Messagebox.ERROR);
						} else {
							// tính tiền trọ, tính số ngày ở lại rồi nhân với số tiền
							// trọ 1 ngày
							Calendar c1 = Calendar.getInstance();
							Calendar c2 = Calendar.getInstance();
							c1.setTime(getNgayBatDau());
							c2.setTime(getNgayKetThuc());
							long money = ((c2.getTime().getTime() - c1.getTime().getTime()) / (24 * 3600 * 1000)) * 15000;
							setSoTien(Long.toString(money));
							// .tính tiền trọ, tiền trọ 1 ngày là 15000 đ
							save();
							Messagebox.show(
									"Số tiền phải trả là: " + getSoTien()
											+ " VNĐ, mời bạn lên ban quản lý khu nhà: "+phongtro.getHouse().getManager().getHoVaTen()+" để nộp tiền và lập hợp đồng",
									"Thông báo", Messagebox.OK, Messagebox.INFORMATION);
							/*BindUtils.postNotifyChange(null, null, this, "*");
							BindUtils.postNotifyChange(null, null, listObject, attr);*/
						}
					}
				}

			} else {
				 Messagebox.show("Chưa chọn phòng", "Cảnh báo", Messagebox.OK,
				            Messagebox.ERROR);
				System.out.println("Chưa chọn phòng");
			}

		} else {
			if (!ngayKetThuc.after(ngayBatDau) && ngayBatDau.before(new Date())) {
				Messagebox.show("Ngày bắt đầu ở phải là ngày hôm nay hoặc lớn hơn và ngày kết thúc phải sau ngày bắt đầu ở, mời chọn lại thời gian", "Cảnh báo", Messagebox.OK,
			            Messagebox.ERROR);
			} else{
				if(!ngayKetThuc.after(ngayBatDau)){
					Messagebox.show("Ngày kết thúc phải sau ngày bắt đầu ở, mời chọn lại thời gian", "Cảnh báo", Messagebox.OK,
				            Messagebox.ERROR);
				}
				if(ngayBatDau.before(new Date())){
					Messagebox.show("Ngày bắt đầu ở phải là ngày hôm nay hoặc lớn hơn, mời chọn lại thời gian", "Cảnh báo", Messagebox.OK,
				            Messagebox.ERROR);
				}
			}
		}
	}

	@Command
	public void huyHopDong(@BindingParam("list") final Object listObject, @BindingParam("attr") final String attr,
			@BindingParam("wdn") final Window wdn) throws IOException {
		setNgayHuyHD(new Date());
		save();
		wdn.detach();
		BindUtils.postNotifyChange(null, null, this, "*");
		BindUtils.postNotifyChange(null, null, listObject, attr);
	}

	@Transient
	public AbstractValidator getvalidatorHopDong() {
		return new AbstractValidator() {
			@Override
			public void validate(final ValidationContext ctx) {
				// validator hop dong
			}
		};
	}

	@Transient
	public int getSoNgayTreHD() {
		int soNgayTreHD = 0;
		Calendar c1 = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();
		c1.setTime(getNgayKetThuc());
		c2.setTime(new Date());
		soNgayTreHD = (int) ((c2.getTime().getTime() - c1.getTime().getTime()) / (24 * 3600 * 1000));
		return soNgayTreHD;
	}
	@Transient
	public int getSoTienDenBu() {
		return getSoNgayTreHD()*15000;
	}
}
