package tcx.hg.gg.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.jasypt.util.password.BasicPasswordEncryptor;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.sys.ValidationMessages;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.image.AImage;
import org.zkoss.image.Image;
import org.zkoss.io.Files;
import org.zkoss.util.media.Media;
import org.zkoss.util.resource.Labels;
import org.zkoss.zul.Window;

import com.google.common.base.Strings;
import com.querydsl.jpa.impl.JPAQuery;

import tcx.hg.model.Model;
import tcx.hg.model.NhanVien;
import tcx.hg.model.QNhanVien;

@Entity
@Table(name = "SinhVien")
public class SinhVien extends Model<SinhVien>{
	public static transient final Logger LOG = LogManager.getLogger(SinhVien.class.getName());
	private String hoVaTen = "";
	private String maSV = "";
	private String gioiTinh = "";
	private Date ngaySinh;
	private String soDienThoai = "";
	private String email = "";
	private String diaChi = "";
	private String tenDangNhap = "";
	private String salkey = "";
	private String matKhau = "";
	private String matKhau2 = "";
	private  Image imageContent = null;
	private String nameAvatar = "";
	private String sinhvienUrl = "";
	
	public String getHoVaTen() {
		return hoVaTen;
	}
	public void setHoVaTen(String hoVaTen) {
		this.hoVaTen = hoVaTen;
	}
	public String getMaSV() {
		return maSV;
	}
	public void setMaSV(String maSV) {
		this.maSV = maSV;
	}
	public String getGioiTinh() {
		return gioiTinh;
	}
	public void setGioiTinh(String gioiTinh) {
		this.gioiTinh = gioiTinh;
	}
	public Date getNgaySinh() {
		return ngaySinh;
	}
	public void setNgaySinh(Date ngaySinh) {
		this.ngaySinh = ngaySinh;
	}
	public String getSoDienThoai() {
		return soDienThoai;
	}
	public void setSoDienThoai(String soDienThoai) {
		this.soDienThoai = soDienThoai;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDiaChi() {
		return diaChi;
	}
	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}
	public String getTenDangNhap() {
		return tenDangNhap;
	}
	public void setTenDangNhap(String tenDangNhap) {
		this.tenDangNhap = tenDangNhap;
	}
	public String getSalkey() {
		return salkey;
	}
	public void setSalkey(String salkey) {
		this.salkey = salkey;
	}
	public String getMatKhau() {
		return matKhau;
	}
	public void setMatKhau(String matKhau) {
		this.matKhau = matKhau;
	}
	public String getMatKhau2() {
		return matKhau2;
	}
	public void setMatKhau2(String matKhau2) {
		this.matKhau2 = matKhau2;
	}
	
	public String getCookieToken(long expire) {
		String token = getId() + ":" + expire + ":";
		return Base64.encodeBase64String(token.concat(DigestUtils.md5Hex(token + matKhau + ":" + salkey)).getBytes());
	}
	
	@Command
	public void saveSinhVien(@BindingParam("list") final Object listObject,
			@BindingParam("attr") final String attr,
			@BindingParam("wdn") final Window wdn) throws IOException {
		LOG.info("save image");
		if (matKhau2 != null && !matKhau2.isEmpty()) {
			updatePassword(matKhau2);
		}
		if (beforeSaveImg()) {
			save();
			wdn.detach();
			BindUtils.postNotifyChange(null, null, this, "*");
			BindUtils.postNotifyChange(null, null, listObject, attr);
		} else {
			showNotification("Bạn chưa chọn hình ảnh", "", "error");
		}
		
	}
	public void updatePassword(String pass) {
		BasicPasswordEncryptor encryptor = new BasicPasswordEncryptor();
		String salkey = getSalkey();
		if (salkey == null || salkey.equals("")) {
			salkey = encryptor.encryptPassword((new Date()).toString());
		}
		String passNoHash = pass + salkey;
		String passHash = encryptor.encryptPassword(passNoHash);
		setSalkey(salkey);
		setMatKhau(passHash);
	}
	
	//load hình ảnh
	public String getSinhVienUrl() {
		return sinhvienUrl;
	}

	public void setSinhVienUrl( String bannerUrl1) {
		this.sinhvienUrl = Strings.nullToEmpty(bannerUrl1);
	}
	
	public String getNameAvatar() {
		return nameAvatar;
	}

	public void setNameAvatar( String _name) {
		this.nameAvatar = Strings.nullToEmpty(_name);
	}
	
	private boolean flagImage = true;
	@Transient
	public  org.zkoss.image.Image getImageContent() {
		if (imageContent == null && !noId()
				&& !core().TT_DA_XOA.equals(getTrangThai())) {
			if (flagImage) {

				try {
					loadImageIsView();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return imageContent;
	}

	public void setImageContent( org.zkoss.image.Image _imageContent) {
		this.imageContent = _imageContent;
	}
	private void loadImageIsView() throws FileNotFoundException, IOException {
		String imgName = "";
		String path = "";
		path = folderStore() + getNameAvatar();
		if (!"".equals(getSinhVienUrl()) && new File(path).exists()) {
			
			try (FileInputStream fis = new FileInputStream(new File(path));){
				setImageContent(new AImage(imgName, fis));
			}
		}
	}
	@Command
	public void attachImages(@BindingParam("media") final Media media,
			@BindingParam("vmsgs")  final ValidationMessages vmsgs) {
		LOG.info("attachImages");
		if (media instanceof org.zkoss.image.Image) {
			if(media.getName().toLowerCase().endsWith(".png")
				|| media.getName().toLowerCase().endsWith(".jpg")){
				int lengthOfImage = media.getByteData().length;
				if (lengthOfImage > 10000000) {
			        showNotification("Chọn hình ảnh có dung lượng nhỏ hơn 10MB.", "", "error");
			        return;
				}
				else{
					String tenFile = media.getName();
					tenFile = tenFile.replace(" ", "");
					tenFile = tenFile.substring(0, tenFile.lastIndexOf(".")) + "_"
							+ Calendar.getInstance().getTimeInMillis()
							+ tenFile.substring(tenFile.lastIndexOf("."));
					setImageContent((org.zkoss.image.Image) media);			
					setNameAvatar(tenFile);
					if (vmsgs != null) {
						vmsgs.clearKeyMessages("errLabel");
					}
					BindUtils.postNotifyChange(null, null, this, "imageContent");
					BindUtils.postNotifyChange(null, null, this, "name");
				}
			} else {
				showNotification("Chọn hình ảnh theo đúng định dạng (*.png, *.jpg)","","error");
			}
		} else {
			showNotification("File tải lên không phải hình ảnh!", "", "error");
		}
	}
	@Command
	public void deleteImg() {
		LOG.info("deleteImg--");
		setImageContent(null);
		setNameAvatar("");
		flagImage = false;
		BindUtils.postNotifyChange(null, null, this, "imageContent");
		BindUtils.postNotifyChange(null, null, this, "name");
	}
	private boolean beforeSaveImg() throws IOException {
		if (getImageContent() == null) {
			return false;
		}
		saveImageToServer();
		return true;
	}

	protected void saveImageToServer() throws IOException {
		
		Image imageContent2 = getImageContent();
		if (imageContent2 != null) {
			// luu hinh
			LOG.info("saveImage() :" + folderStore() + getNameAvatar());
			
			setSinhVienUrl(folderSinhVienUrl().concat(getNameAvatar()));
			final File baseDir = new File(folderStore().concat(getNameAvatar()));
			Files.copy(baseDir, imageContent2.getStreamData());
		}
	}
	
	@Transient
	public String folderSinhVienUrl() {
		return "/" + Labels.getLabel("filestore.folder") + "/sinhvien/";
	}
	
	//validate
	@Transient
	public AbstractValidator getValidatorEmail() {
		return new AbstractValidator() {
			@Override
			public void validate(final ValidationContext ctx) {
				String value = (String)ctx.getProperty().getValue();
				if(value == null || "".equals(value)) {
					addInvalidMessage(ctx, "error","Không được để trống trường này");
				}
				else if(!value.trim().matches(".+@.+\\.[a-z]+"))
				{
					addInvalidMessage(ctx, "error","Email không đúng định dạng");
				}
				else 
				{
					JPAQuery<SinhVien> q = find(SinhVien.class)
							.where(QSinhVien.sinhVien.email.eq(value))
							.where(QSinhVien.sinhVien.trangThai.ne(core().TT_DA_XOA));
					if(!SinhVien.this.noId()) {
						q.where(QSinhVien.sinhVien.id.ne(getId()));
					}
					if(q.fetchCount() > 0) {
						addInvalidMessage(ctx, "error","Email đã được sử dụng");
					}
				}
			}
		};
	}
	@Transient
	public AbstractValidator getValidatorMaSV() {
		return new AbstractValidator() {
			@Override
			public void validate(final ValidationContext ctx) {
				String value = (String)ctx.getProperty().getValue();
				if(value == null || "".equals(value)) {
					addInvalidMessage(ctx, "error","Không được để trống trường này");
				}
				else 
				{
					JPAQuery<SinhVien> q = find(SinhVien.class)
							.where(QSinhVien.sinhVien.maSV.eq(value))
							.where(QSinhVien.sinhVien.trangThai.ne(core().TT_DA_XOA));
					if(!SinhVien.this.noId()) {
						q.where(QSinhVien.sinhVien.id.ne(getId()));
					}
					if(q.fetchCount() > 0) {
						addInvalidMessage(ctx, "error","Mã sinh viên đã được sử dụng");
					}
				}
			}
		};
	}
	@Transient
	public AbstractValidator getValidatorSinhVien() {
		return new AbstractValidator() {
			@Override
			public void validate(final  ValidationContext ctx) {
				if (getImageContent() == null) {
					addInvalidMessage(ctx, "error",
							"Bạn chưa chọn hình ảnh đại diện.");
				}
			}
		};
	}

}
