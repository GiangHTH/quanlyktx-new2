package tcx.hg.gg.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import tcx.hg.model.Model;

@Entity
@Table (name="chude")
public class ChuDe extends Model<ChuDe> {
	public static transient final Logger LOG = LogManager.getLogger(ChuDe.class.getName());
	
	private String ten = "";
	private String mota = "";
	private  ChuDe chudecha;
	
	public String getTen() {
		return ten;
	}
	public void setTen(String ten) {
		this.ten = ten;
	}
	
	public String getMota() {
		return mota;
	}
	public void setMota(String mota) {
		this.mota = mota;
	}
	
	@ManyToOne
	public ChuDe getChudecha() {
		return chudecha;
	}
	public void setChudecha(ChuDe chudecha) {
		this.chudecha = chudecha;
	}
	
	
}
