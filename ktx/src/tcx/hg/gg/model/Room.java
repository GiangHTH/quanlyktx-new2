package tcx.hg.gg.model;

import java.io.IOException;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.zul.Window;

import tcx.hg.model.Model;

@Entity
@Table(name = "Room")
public class Room extends Model<Room>{
	private String name = "";
	
	private String description = "";
	
	private int maxPeople = 0;
	
	private House house;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getMaxPeople() {
		return maxPeople;
	}

	public void setMaxPeople(int maxPeople) {
		this.maxPeople = maxPeople;
	}

	@ManyToOne
	public House getHouse() {
		return house;
	}

	public void setHouse(House house) {
		this.house = house;
	}
	
	@Command
	public void saveRoom(@BindingParam("list") final Object listObject,
			@BindingParam("attr") final String attr,
			@BindingParam("wdn") final Window wdn) throws IOException{
		save();
		wdn.detach();
		BindUtils.postNotifyChange(null, null, listObject, attr);
	}
	
	@Transient
	public AbstractValidator getValidatorHouse() {
		return new AbstractValidator() {
			@Override
			public void validate(final  ValidationContext ctx) {
				/*if (getImageContent() == null) {
					addInvalidMessage(ctx, "error",
							"Bạn chưa chọn hình ảnh cho banner.");
				}
				Date fromDate = getNgayBatDau();
				Date toDate = getNgayHetHan();
				if (fromDate != null && toDate != null) {
					if (fromDate.compareTo(toDate) > 0) {
						addInvalidMessage(ctx, "lblErr",
								"Ngày hết hạn phải lớn hơn hoặc bằng ngày bắt đầu.");
					}
				}*/
			}
		};
	}
	
}
