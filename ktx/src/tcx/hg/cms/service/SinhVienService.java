package tcx.hg.cms.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.jasypt.util.password.BasicPasswordEncryptor;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;

import com.querydsl.jpa.impl.JPAQuery;

import tcx.hg.gg.model.QRoom;
import tcx.hg.gg.model.QSinhVien;
import tcx.hg.gg.model.Room;
import tcx.hg.gg.model.SinhVien;
import tcx.hg.model.NhanVien;
import tcx.hg.model.QNhanVien;
import tcx.hg.service.BasicService;

public class SinhVienService extends BasicService<SinhVien>{
	public SinhVien getSinhVien(boolean saving) {
		if (Executions.getCurrent() == null) {
			return null;
		}
		return getSinhVien(saving, (HttpServletRequest) Executions.getCurrent().getNativeRequest(),
				(HttpServletResponse) Executions.getCurrent().getNativeResponse());
	}
	
	public JPAQuery<SinhVien> getTargetQuery() {
		String paramTrangThai = MapUtils.getString(argDeco(), Labels.getLabel("param.trangthai"), "").trim();
		String tuKhoa = MapUtils.getString(argDeco(), Labels.getLabel("param.tukhoa"), "").trim();
		String maSV = MapUtils.getString(argDeco(), Labels.getLabel("param.masv"), "").trim();
		String gioiTinh = MapUtils.getString(argDeco(), Labels.getLabel("param.gioitinh"), "").trim();
		JPAQuery<SinhVien> q = find(SinhVien.class).where(QSinhVien.sinhVien.trangThai.ne(core().TT_DA_XOA));

		if (tuKhoa != null && !tuKhoa.isEmpty()) {
			q.where(QSinhVien.sinhVien.hoVaTen.containsIgnoreCase(tuKhoa)
					.or(QSinhVien.sinhVien.tenDangNhap.containsIgnoreCase(tuKhoa)));
		}
		
		if (maSV != null && !maSV.isEmpty()) {
			q.where(QSinhVien.sinhVien.maSV.containsIgnoreCase(maSV));
		}
		
		if (gioiTinh != null && !gioiTinh.isEmpty()) {
			q.where(QSinhVien.sinhVien.gioiTinh.eq(gioiTinh));
		}

		if (paramTrangThai != null && !paramTrangThai.isEmpty()) {
			q.where(QSinhVien.sinhVien.trangThai.eq(paramTrangThai));
		}
		q.orderBy(QSinhVien.sinhVien.trangThai.asc());
		return q.orderBy(QSinhVien.sinhVien.ngaySua.desc());
	}
	
	public Map<String, String> getListAllGioiTinhAndNull(){
		HashMap<String, String> result = new HashMap<>();
		result.put(null, "");
		result.put("nam", "Nam");
		result.put("nu", "Nữ");
		result.put("chua_xac_dinh", "Chưa xác định");
		return result;
	}
	
	public Map<Long, String> getListAllRoomAndNull(){
		HashMap<Long, String> result = new HashMap<>();
		result.put(null, "");
		List<Room> listAllRoom = new ArrayList<Room>();
		listAllRoom.addAll(find(Room.class).where(QRoom.room.trangThai.ne(core().TT_DA_XOA)).fetch());
		for (Room room : listAllRoom) {
			result.put(Long.valueOf(room.getId()), room.getName());
		}
		return result;
	}
	
	@Command
	public void login(@BindingParam("email") final String email, @BindingParam("password") final String password) {
		// System.out.println("email: " + email);
		// System.out.println("password: " + password);
		SinhVien sinhVien = new JPAQuery<SinhVien>(em()).from(QSinhVien.sinhVien)
				.where(QSinhVien.sinhVien.daXoa.isFalse()).where(QSinhVien.sinhVien.trangThai.ne(core().TT_DA_XOA))
				.where(QSinhVien.sinhVien.tenDangNhap.eq(email))
				.fetchFirst();
		BasicPasswordEncryptor encryptor = new BasicPasswordEncryptor();
		if (sinhVien != null
				&& encryptor.checkPassword(password.trim() + sinhVien.getSalkey(), sinhVien.getMatKhau())) {
			// System.out.println("nhan vien != null");
			String cookieToken = sinhVien
					.getCookieToken(System.currentTimeMillis() + TimeUnit.MILLISECONDS.convert(6, TimeUnit.HOURS));
			Session zkSession = Sessions.getCurrent();
			zkSession.setAttribute("email", cookieToken);
			HttpServletResponse res = (HttpServletResponse) Executions.getCurrent().getNativeResponse();
			Cookie cookie = new Cookie("email", cookieToken);
			cookie.setPath("/");
			cookie.setMaxAge(1000000000);
			res.addCookie(cookie);
			Executions.sendRedirect("/");
		} else {
			showNotification("Đăng nhập không thành công", "", "error");
		}
	}

	@Command
	public void logout() {
		// System.out.println("logout");
		SinhVien SinhVienLogin = getSinhVien(true);
		if (SinhVienLogin != null && !SinhVienLogin.noId()) {
			Session zkSession = Sessions.getCurrent();
			zkSession.removeAttribute("email");
			HttpServletResponse res = (HttpServletResponse) Executions.getCurrent().getNativeResponse();
			Cookie cookie = new Cookie("email", null);
			cookie.setPath("/");
			cookie.setMaxAge(0);
			res.addCookie(cookie);
			Executions.sendRedirect("/");
		}
	}
}
