package tcx.hg.cms.service;

import org.apache.commons.collections.MapUtils;
import org.zkoss.util.resource.Labels;

import com.querydsl.jpa.impl.JPAQuery;

import tcx.hg.gg.model.ChuDe;
import tcx.hg.gg.model.QChuDe;
import tcx.hg.service.BasicService;

public class ChuDeService extends BasicService<ChuDe> {
	public JPAQuery<ChuDe> getTargetQuery() {
		String paramImage = MapUtils.getString(argDeco(),Labels.getLabel("param.tukhoa"),"").trim();
		System.out.println(paramImage);
		JPAQuery<ChuDe> q = find(ChuDe.class)
				.where(QChuDe.chuDe.trangThai.ne(core().TT_DA_XOA));
		if (paramImage != null && !paramImage.isEmpty()) {
			String tukhoa = "%" + paramImage + "%";
			q.where(QChuDe.chuDe.ten.like(tukhoa)
				.or(QChuDe.chuDe.mota.like(tukhoa)));
		}
		
		q.orderBy(QChuDe.chuDe.ngaySua.desc());
		return q;
	}
}
