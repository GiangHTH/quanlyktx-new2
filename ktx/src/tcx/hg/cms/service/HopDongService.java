package tcx.hg.cms.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.MapUtils;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.util.ForEachStatus;

import com.liferay.portal.kernel.util.Tuple;
import com.querydsl.jpa.impl.JPAQuery;

import tcx.hg.gg.model.HopDong;
import tcx.hg.gg.model.QHopDong;
import tcx.hg.gg.model.QRoom;
import tcx.hg.gg.model.Room;
import tcx.hg.gg.model.SinhVien;
import tcx.hg.model.NhanVien;
import tcx.hg.model.QNhanVien;
import tcx.hg.service.BasicService;

public class HopDongService extends BasicService<HopDong>{
	public JPAQuery<HopDong> getTargetQuery() {
		String paramImage = MapUtils.getString(argDeco(),Labels.getLabel("param.tukhoa"),"").trim();
		String trangThai = MapUtils.getString(argDeco(), Labels.getLabel("param.trangthai"),"");
		System.out.println(paramImage);
		System.out.println(trangThai);
		JPAQuery<HopDong> q = find(HopDong.class)
				.where(QHopDong.hopDong.trangThai.ne(core().TT_DA_XOA));
		if (paramImage != null && !paramImage.isEmpty()) {
			String tukhoa = "%" + paramImage + "%";
			q.where(QHopDong.hopDong.sinhvien.hoVaTen.like(tukhoa)
				.or(QHopDong.hopDong.sinhvien.maSV.like(tukhoa)));
		}
		if (!trangThai.isEmpty() || trangThai != "" || trangThai != null) {
			if (trangThai.equals("da_lap_hd")) {
				q.where(QHopDong.hopDong.ngayTaoHD.isNotNull());
			}
			if (trangThai.equals("chua_lap_hd")) {
				q.where(QHopDong.hopDong.ngayTaoHD.isNull());
			}
		}
		
		if (getNgayBatDau() != null && getNgayKetThuc() != null ) {
			q.where(QHopDong.hopDong.ngayTaoHD.after(getNgayBatDau()).or(QHopDong.hopDong.ngayTaoHD.eq(getNgayBatDau())),
					QHopDong.hopDong.ngayTaoHD.before(getNgayKetThuc()).or(QHopDong.hopDong.ngayTaoHD.eq(getNgayKetThuc())));
		}
		
		if (getNgayBatDau() == null && getNgayKetThuc() != null) {
			q.where(QHopDong.hopDong.ngayTaoHD.before(getNgayKetThuc()).or(QHopDong.hopDong.ngayTaoHD.eq(getNgayKetThuc())));
		}
		
		if (getNgayBatDau() != null && getNgayKetThuc() == null ) {
			q.where(QHopDong.hopDong.ngayTaoHD.after(getNgayBatDau()).or(QHopDong.hopDong.ngayTaoHD.eq(getNgayBatDau())));
		}
		
		q.orderBy(QHopDong.hopDong.id.desc());
		return q;
	}
	
	public JPAQuery<HopDong> getTargetQueryDKi(SinhVien sinhvien) {
		JPAQuery<HopDong> q = find(HopDong.class)
				.where(QHopDong.hopDong.trangThai.ne(core().TT_DA_XOA))
				.where(QHopDong.hopDong.sinhvien.id.eq(sinhvien.getId()))
				.where(QHopDong.hopDong.ngayTaoHD.isNull())
				.where(QHopDong.hopDong.ngayHuyHD.isNull());
		
		q.orderBy(QHopDong.hopDong.ngayTao.desc());
		return q;
	}
	public JPAQuery<HopDong> getTargetQueryHDong(SinhVien sinhvien) {
		JPAQuery<HopDong> q = find(HopDong.class)
				.where(QHopDong.hopDong.trangThai.ne(core().TT_DA_XOA))
				.where(QHopDong.hopDong.sinhvien.id.eq(sinhvien.getId()))
				.where(QHopDong.hopDong.ngayTaoHD.isNotNull());
		q.orderBy(QHopDong.hopDong.ngayTaoHD.desc());
		System.out.println("đây đây "+ q.fetchCount());
		return q;
	}
	

	private List<HopDong> hopDongQuaHan = new ArrayList<>();
	public List<HopDong> getHopDongQuaHan() {
		hopDongQuaHan.addAll(find(HopDong.class).where(QHopDong.hopDong.trangThai.ne(core().TT_DA_XOA))
				.where(QHopDong.hopDong.ngayTaoHD.isNotNull())
				.where(QHopDong.hopDong.ngayHuyHD.isNull())
				.where(QHopDong.hopDong.ngayKetThuc.before(new Date())).fetch());
		return hopDongQuaHan;
	}
	
	public HopDong hopDongNow(SinhVien sv){
		JPAQuery<HopDong> q = find(HopDong.class)
				.where(QHopDong.hopDong.trangThai.ne(core().TT_DA_XOA))
				.where(QHopDong.hopDong.sinhvien.id.eq(sv.getId()))
				.where(QHopDong.hopDong.ngayTaoHD.isNotNull())
				.where(QHopDong.hopDong.ngayHuyHD.isNull());
		HopDong hdn = q.fetchOne();
		return hdn;
	}

}
