package tcx.hg.cms.service;

import java.util.ArrayList;

import org.apache.commons.collections.MapUtils;
import org.zkoss.util.resource.Labels;

import com.querydsl.core.Tuple;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.NumberExpression;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQuery;

import java.util.List;

import tcx.hg.gg.model.HopDong;
import tcx.hg.gg.model.House;
import tcx.hg.gg.model.QHopDong;
import tcx.hg.gg.model.QRoom;
import tcx.hg.gg.model.Room;
import tcx.hg.service.BasicService;

public class RoomService extends BasicService<Room>{
	private String img = "/backend/assets/img/edit.png";
	private String hoverImg = "/backend/assets/img/edit_hover.png";
	/*private String strUpdate = "Thứ tự";*/
	private boolean update = true;
	private boolean updateThanhCong = true;
	
	public JPAQuery<Room> getTargetQuery() {
		String paramImage = MapUtils.getString(argDeco(),Labels.getLabel("param.tukhoa"),"").trim();
		String trangThai = MapUtils.getString(argDeco(), Labels.getLabel("param.trangthai"),"");
		long soNguoiMax = MapUtils.getLongValue(argDeco(), Labels.getLabel("param.songuoimax"));
		long house = MapUtils.getLongValue(argDeco(), Labels.getLabel("param.house"));
		
		System.out.println(paramImage);
		System.out.println(trangThai);
		System.out.println(soNguoiMax);
		System.out.println(house);
		
		JPAQuery<Room> q = find(Room.class)
				.where(QRoom.room.trangThai.ne(core().TT_DA_XOA));
		if (paramImage != null && !paramImage.isEmpty()) {
			String tukhoa = "%" + paramImage + "%";
			q.where(QRoom.room.name.like(tukhoa)
				.or(QRoom.room.description.like(tukhoa)));
			
		}
		if (!trangThai.isEmpty()) {
			q.where(QRoom.room.trangThai.eq(trangThai));
		}
		
		if (soNguoiMax > 0) {
			Room room = em().find(Room.class, soNguoiMax);
			q.where(QRoom.room.maxPeople.eq(room.getMaxPeople()));
		}
		
		if (house > 0) {
			House h = em().find(House.class, house);
			q.where(QRoom.room.house.eq(h));
		}
		
		q.orderBy(QRoom.room.id.asc());
		return q;
	}
	
	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getHoverImg() {
		return hoverImg;
	}

	public void setHoverImg(String hoverImg) {
		this.hoverImg = hoverImg;
	}

	public boolean isUpdate() {
		return update;
	}

	public void setUpdate(boolean update) {
		this.update = update;
	}

	public boolean isUpdateThanhCong() {
		return updateThanhCong;
	}

	public void setUpdateThanhCong(boolean updateThanhCong) {
		this.updateThanhCong = updateThanhCong;
	}
	
	private List<Room> soNguoiMaxList;
	public List<Room> getSoNguoiMaxList(){
		soNguoiMaxList = new ArrayList<>();
		soNguoiMaxList.add(null);
		soNguoiMaxList.addAll(getTargetQuery().fetch());
		return soNguoiMaxList;
	}

	public long countLiving(long idphong) {
		JPAQuery q= find(HopDong.class)
				.where(QHopDong.hopDong.trangThai.ne(core().TT_DA_XOA))
				.where(QHopDong.hopDong.phongtro.id.eq(idphong))
				.where(QHopDong.hopDong.ngayTaoHD.isNotNull())
				.where(QHopDong.hopDong.ngayHuyHD.isNull());

		return q.fetchCount();
	}
	public long countRegisted(long idphong) {
		JPAQuery q= find(HopDong.class)
				.where(QHopDong.hopDong.trangThai.ne(core().TT_DA_XOA))
				.where(QHopDong.hopDong.phongtro.id.eq(idphong))
				.where(QHopDong.hopDong.ngayTaoHD.isNull())
				.where(QHopDong.hopDong.ngayHuyHD.isNull());

		return q.fetchCount();
	}
	//left join 2 bảng
	public List test() {		
		QHopDong hopDong = QHopDong.hopDong;		
		QRoom room = QRoom.room;		
		JPAQuery q = find(Room.class);		
		q.leftJoin(hopDong).on(room.eq(hopDong.phongtro));		
		q.groupBy(room);		
		List l = new ArrayList();
		//đếm số id hợp đồng theo room
		l = q.select(room, hopDong.id.count()).fetch();		
		/*System.out.println("---------------------------------------------------------------");	*/	
		/*for(Tuple tup : l) {			
			System.out.println(tup.get(0, Object.class) + " | " + tup.get(1, Long.class));			
			System.out.println("---------------------------------------------------------------");		
			}	*/
		return l;
	}
}
