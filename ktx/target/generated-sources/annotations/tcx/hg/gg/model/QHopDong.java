package tcx.hg.gg.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QHopDong is a Querydsl query type for HopDong
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QHopDong extends EntityPathBase<HopDong> {

    private static final long serialVersionUID = 371479684L;

    private static final PathInits INITS = new PathInits("*", "nguoiSua.*.*.*.*", "nguoiTao.*.*.*.*");

    public static final QHopDong hopDong = new QHopDong("hopDong");

    public final tcx.hg.model.QModel _super;

    //inherited
    public final BooleanPath daXoa;

    public final StringPath ghiChu = createString("ghiChu");

    //inherited
    public final NumberPath<Long> id;

    public final DateTimePath<java.util.Date> ngayBatDau = createDateTime("ngayBatDau", java.util.Date.class);

    public final DateTimePath<java.util.Date> ngayHuyHD = createDateTime("ngayHuyHD", java.util.Date.class);

    public final DateTimePath<java.util.Date> ngayKetThuc = createDateTime("ngayKetThuc", java.util.Date.class);

    //inherited
    public final DateTimePath<java.util.Date> ngaySua;

    //inherited
    public final DateTimePath<java.util.Date> ngayTao;

    public final DateTimePath<java.util.Date> ngayTaoHD = createDateTime("ngayTaoHD", java.util.Date.class);

    // inherited
    public final tcx.hg.model.QNhanVien nguoiSua;

    // inherited
    public final tcx.hg.model.QNhanVien nguoiTao;

    public final QRoom phongtro;

    public final QSinhVien sinhvien;

    public final StringPath soTien = createString("soTien");

    //inherited
    public final StringPath trangThai;

    public QHopDong(String variable) {
        this(HopDong.class, forVariable(variable), INITS);
    }

    public QHopDong(Path<? extends HopDong> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QHopDong(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QHopDong(PathMetadata metadata, PathInits inits) {
        this(HopDong.class, metadata, inits);
    }

    public QHopDong(Class<? extends HopDong> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this._super = new tcx.hg.model.QModel(type, metadata, inits);
        this.daXoa = _super.daXoa;
        this.id = _super.id;
        this.ngaySua = _super.ngaySua;
        this.ngayTao = _super.ngayTao;
        this.nguoiSua = _super.nguoiSua;
        this.nguoiTao = _super.nguoiTao;
        this.phongtro = inits.isInitialized("phongtro") ? new QRoom(forProperty("phongtro"), inits.get("phongtro")) : null;
        this.sinhvien = inits.isInitialized("sinhvien") ? new QSinhVien(forProperty("sinhvien"), inits.get("sinhvien")) : null;
        this.trangThai = _super.trangThai;
    }

}

