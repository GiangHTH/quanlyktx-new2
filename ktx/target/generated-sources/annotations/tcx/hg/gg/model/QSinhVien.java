package tcx.hg.gg.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QSinhVien is a Querydsl query type for SinhVien
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSinhVien extends EntityPathBase<SinhVien> {

    private static final long serialVersionUID = -432803051L;

    private static final PathInits INITS = new PathInits("*", "nguoiSua.*.*.*.*", "nguoiTao.*.*.*.*");

    public static final QSinhVien sinhVien = new QSinhVien("sinhVien");

    public final tcx.hg.model.QModel _super;

    //inherited
    public final BooleanPath daXoa;

    public final StringPath diaChi = createString("diaChi");

    public final StringPath email = createString("email");

    public final StringPath gioiTinh = createString("gioiTinh");

    public final StringPath hoVaTen = createString("hoVaTen");

    //inherited
    public final NumberPath<Long> id;

    public final StringPath maSV = createString("maSV");

    public final StringPath matKhau = createString("matKhau");

    public final StringPath matKhau2 = createString("matKhau2");

    public final StringPath nameAvatar = createString("nameAvatar");

    public final DateTimePath<java.util.Date> ngaySinh = createDateTime("ngaySinh", java.util.Date.class);

    //inherited
    public final DateTimePath<java.util.Date> ngaySua;

    //inherited
    public final DateTimePath<java.util.Date> ngayTao;

    // inherited
    public final tcx.hg.model.QNhanVien nguoiSua;

    // inherited
    public final tcx.hg.model.QNhanVien nguoiTao;

    public final StringPath salkey = createString("salkey");

    public final StringPath sinhVienUrl = createString("sinhVienUrl");

    public final StringPath soDienThoai = createString("soDienThoai");

    public final StringPath tenDangNhap = createString("tenDangNhap");

    //inherited
    public final StringPath trangThai;

    public QSinhVien(String variable) {
        this(SinhVien.class, forVariable(variable), INITS);
    }

    public QSinhVien(Path<? extends SinhVien> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QSinhVien(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QSinhVien(PathMetadata metadata, PathInits inits) {
        this(SinhVien.class, metadata, inits);
    }

    public QSinhVien(Class<? extends SinhVien> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this._super = new tcx.hg.model.QModel(type, metadata, inits);
        this.daXoa = _super.daXoa;
        this.id = _super.id;
        this.ngaySua = _super.ngaySua;
        this.ngayTao = _super.ngayTao;
        this.nguoiSua = _super.nguoiSua;
        this.nguoiTao = _super.nguoiTao;
        this.trangThai = _super.trangThai;
    }

}

