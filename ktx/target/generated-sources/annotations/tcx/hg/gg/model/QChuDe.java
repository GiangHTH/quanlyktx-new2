package tcx.hg.gg.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QChuDe is a Querydsl query type for ChuDe
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QChuDe extends EntityPathBase<ChuDe> {

    private static final long serialVersionUID = -1018958808L;

    private static final PathInits INITS = new PathInits("*", "nguoiSua.*.*.*.*", "nguoiTao.*.*.*.*");

    public static final QChuDe chuDe = new QChuDe("chuDe");

    public final tcx.hg.model.QModel _super;

    public final QChuDe chudecha;

    //inherited
    public final BooleanPath daXoa;

    //inherited
    public final NumberPath<Long> id;

    public final StringPath mota = createString("mota");

    //inherited
    public final DateTimePath<java.util.Date> ngaySua;

    //inherited
    public final DateTimePath<java.util.Date> ngayTao;

    // inherited
    public final tcx.hg.model.QNhanVien nguoiSua;

    // inherited
    public final tcx.hg.model.QNhanVien nguoiTao;

    public final StringPath ten = createString("ten");

    //inherited
    public final StringPath trangThai;

    public QChuDe(String variable) {
        this(ChuDe.class, forVariable(variable), INITS);
    }

    public QChuDe(Path<? extends ChuDe> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QChuDe(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QChuDe(PathMetadata metadata, PathInits inits) {
        this(ChuDe.class, metadata, inits);
    }

    public QChuDe(Class<? extends ChuDe> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this._super = new tcx.hg.model.QModel(type, metadata, inits);
        this.chudecha = inits.isInitialized("chudecha") ? new QChuDe(forProperty("chudecha"), inits.get("chudecha")) : null;
        this.daXoa = _super.daXoa;
        this.id = _super.id;
        this.ngaySua = _super.ngaySua;
        this.ngayTao = _super.ngayTao;
        this.nguoiSua = _super.nguoiSua;
        this.nguoiTao = _super.nguoiTao;
        this.trangThai = _super.trangThai;
    }

}

