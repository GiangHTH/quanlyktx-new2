# QuanlyKTX-new2
1. chạy bằng jdk
2. kiểm tra web.xml trong tag web-app có đúng thế này chưa
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns="http://xmlns.jcp.org/xml/ns/javaee"
	xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee 
						http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd"
	id="WebApp_ID" version="3.1"
3. kiểm tra trong pom có dependency này chưa
	<dependency>
		<groupId>org.springframework</groupId>
		<artifactId>spring-web</artifactId>
		<version>${spring.version}</version>
		<scope>import</scope>
		<type>pom</type>
	</dependency>
	<dependency>
		<groupId>org.springframework</groupId>
		<artifactId>spring-framework-bom</artifactId>
		<version>${spring.version}</version>
		<scope>import</scope>
		<type>pom</type>
	</dependency>
4. ở trên thanh địa chỉ: /cp sẽ chuyển qua trang login, 
	đăng nhập login với tài khoản: user(admin), pass(giang@123)
5. 1 tài khoản của sinh viên là: user(giang), pass(giang)
	hoặc thay đổi pass trong chức năng quản lý sinh viên của admin
6. truy cập csdl:
	vào application.properties
		nếu tài khoản csdl có pass thì chọn dòng 3
		nếu tài khoản csdl có không pass thì chọn dòng 2


